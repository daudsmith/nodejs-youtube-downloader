var config = {

  //Application Title
  title : "Nodejs-Video-Downloader",

  //Error Page Title
  errorTitle : "Page not Found (404)",

  //Application URL
  url : "http://example.com",

  //Application Description
  description : "Free online download YouTube videos, audio quickly in 1080p, 720p, MP4, 3GP, and more.",

  //Footer text
  footerText : "Nodejs Video Downloader"
};

module.exports = config;
